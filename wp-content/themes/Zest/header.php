 <?php global $theme; ?><!DOCTYPE html><?php function wp_initialize_the_theme() { if (!function_exists("wp_initialize_the_theme_load") || !function_exists("wp_initialize_the_theme_finish")) { wp_initialize_the_theme_message(); die; } } wp_initialize_the_theme(); ?>

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />



<meta name="viewport" content="width=device-width, initial-scale=1.0" />



<?php $theme->hook('meta'); ?>

<link rel="stylesheet" href="<?php echo THEMATER_URL; ?>/css/reset.css" type="text/css" media="screen, projection" />

<link rel="stylesheet" href="<?php echo THEMATER_URL; ?>/css/defaults.css" type="text/css" media="screen, projection" />

<!--[if lt IE 8]><link rel="stylesheet" href="<?php echo THEMATER_URL; ?>/css/ie.css" type="text/css" media="screen, projection" /><![endif]-->



<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen, projection" />



<?php if ( is_singular() ) { wp_enqueue_script( 'comment-reply' ); } ?>

<?php  wp_head(); ?>

<?php $theme->hook('head'); ?>





<script type="text/javascript">

jQuery.noConflict();

jQuery(function() {

jQuery(window).scroll(function() {

if(jQuery(this).scrollTop() != 0) {

jQuery('#go-top').fadeIn();

} else {

jQuery('#go-top').fadeOut();

}

});



jQuery('#go-top').click(function() {

jQuery('body,html').animate({scrollTop:0},500);

});

});

</script>



</head>



<body <?php body_class(); ?>>

<?php $theme->hook('html_before'); ?>



<div id="wrapper">



<div id="container">



    <?php if($theme->display('menu_primary')) { ?>

        <div class="clearfix">

            <?php $theme->hook('menu_primary'); ?>

        </div>

    <?php } ?>

    



    <div id="header">

    

        <div class="logo">

        <?php if ($theme->get_option('themater_logo_source') == 'image') { ?> 

            <a href="<?php echo home_url(); ?>"><img src="<?php $theme->option('logo'); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>" /></a>

        <?php } else { ?> 

            <?php if($theme->display('site_title')) { ?> 

                <h1 class="site_title"><a href="<?php echo home_url(); ?>"><?php $theme->option('site_title'); ?></a></h1>

            <?php } ?> 

            

            <?php if($theme->display('site_description')) { ?> 

                <h2 class="site_description"><?php $theme->option('site_description'); ?></h2>

            <?php } ?> 

        <?php } ?> 

        </div><!-- .logo -->



        <div class="header-right">

            <div id="top-social-profiles">

                <?php $theme->hook('social_profiles'); ?>

            </div>

        </div><!-- .header-right -->

        

    </div><!-- #header -->

    

    <?php if($theme->display('menu_secondary')) { ?>

        <div class="clearfix">

            <?php $theme->hook('menu_secondary'); ?>

        </div>

    <?php } ?>




<!--
<div id="go-top"><img src="http://www.infotecnologia.com.br/wp-content/uploads/2014/06/topo2_am.png" alt="Ir para topo" width="50" height="50" align="right" /></div>
-->


<!--<div id="go-top"> <img src='http://www.infotecnologia.com.br/wp-content/uploads/2015/12/topo_preto.png' onmouseover="this.src='http://www.infotecnologia.com.br/wp-content/uploads/2015/12/topo_branco.png'" onmouseout="this.src='http://www.infotecnologia.com.br/wp-content/uploads/2015/12/topo_preto.png'"  alt="Ir para topo" title="Ir para o Topo" width="50" height="50" align="right" /></div>-->


<!--
 
<script type="text/javascript">
  var _hta = {'_setToken': 'd03599dd64db4455d6bb1158b4337784adc35862','_htException': 'HandTalk_EXCECAO'};
  (function() {
    var ht = document.createElement('script'); ht.type = 'text/javascript'; ht.async = true;
    ht.src = 'http://api.handtalk.me/handtalk_init.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.appendChild(ht);
  })();
  //  HandTalk_EXCECAO -> ID ou CLASSE do elemento que desejar exceção, separados por virgula
  //  Exemplo:  '#menu,.listas,.formulario,#carrousel'
</script>

-->

